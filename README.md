# gatsby-tips
* run development server `npm start`
* create production build `npm run build`
* test the build locally `npm run serve` then open [http://localhost:9000/](http://localhost:9000)
* Instead of `__DEV__` we need to use `process.env.NODE_ENV === 'development'`condition.
* any environment variable from `.env.development` or `.env.production` will be available in `process.env.VARIABLE_NAME`

### Adding Routes
* what ever components created inside `src/pages` folder will become the website routes with the component content as body
* for adding any head meta data use `react-helmet`
* for common layout parts like `header` / `footer` use template at `/src/layouts/`

## Further checkpoints
* For any kind of images don't keep url's from s3 or zeplin. Convert images to `webp` format and keep them in `/static/assets/images/` folder.
	* when you want to use them in markup just give the relative path like ```<img src="/assets/images/img.webp" /> ```
* Currently for hyperlinks don't use `gatsby-link` ...it will preload those links and will be loaded in all time and hidden. so instead use normal `anchor` `(<a href='..' />)` tags.

## Configuration
* maintain proper node and npm versions.
	* current project versions are 
	* node : 9.4.0
	* npm : 5.6.0
	* yarn : 1.3.2 

## What's included

Currently this starter includes the following:

* CSS-in-JS via [Emotion](https://github.com/emotion-js/emotion).
* Jest and Enzyme for testing.
* Eslint in dev mode with the airbnb config and prettier formatting rules.
* React 16.
* A basic blog, with posts under src/pages/blog. There's also a script which creates a new Blog entry (post.sh).
* Data per JSON files.
* A few basic components (Navigation, Footer, Layout).
* Layout components make use of [Styled-System](https://github.com/jxnblk/styled-system).
* Google Analytics (you just have to enter your tracking-id).
* Gatsby-Plugin-Offline which includes Service Workers.
* [Prettier](https://github.com/prettier/prettier) for a uniform codebase.
* [Normalize](https://github.com/necolas/normalize.css/) css (7.0).
* [Feather](https://feather.netlify.com/) icons.
* Font styles taken from [Tachyons](http://tachyons.io/).


## How to use it?

If you have 'gatsby' installed globally, just start a new project with:

```
gatsby new my-project https://github.com/saschajullmann/gatsby-starter-gatsbythemes
```

If there is anything else you'd like to see included or changed, just let me
know.
